# Class: sshd
#
# Author: Rodney Brown
#
# Version: 0.4
#
# This module manages the /etc/ssh/sshd_config file using a template
#
# Parameters:
#
# Actions:
#
# Requires:
#
# Sample Usage:
#     
#  class { 'sshd':
#      port => '22',
#  }
#
              class sshd ( $port = "22",
                  $addressfamily = "any",
               $listenaddressip4 = "0.0.0.0",
               $listenaddressip6 = "::",
                       $protocol = "2",
             $protocolonehostkey = "/etc/ssh/ssh_host_key",
          $protocoltwohostkeyrsa = "/etc/ssh/ssh_host_rsa_key",
          $protocoltwohostkeydsa = "/etc/ssh/ssh_host_dsa_key",
        $keyregenerationinterval = "1h",
                  $serverkeybits = "1024",
                 $syslogfacility = "AUTHPRIV",
                       $loglevel = "info",
                 $logingracetime = "120",
                $permitrootlogin = "yes",
                    $strictmodes = "yes",
                   $maxauthtries = "3",
                    $maxsessions = "10",
              $rsaauthentication = "yes",
           $pubkeyauthentication = "yes",
             $authorizedkeysfile = ".ssh/authorized_keys",
        $rhostsrsaauthentication = "no",
        $hostbasedauthentication = "no",
           $ignoreuserknownhosts = "no",
                   $ignorerhosts = "yes",
           $permitemptypasswords = "no",
         $passwordauthentication = "yes",
$challengeresponseauthentication = "no",
         $kerberosauthentication = "no",
          $kerberosorlocalpasswd = "yes",
          $kerberosticketcleanup = "yes",
             $kerberosusekuserok = "yes",
           $gssapiauthentication = "no",
       $gssapicleanupcredentials = "yes",
      $gssapistrictacceptorcheck = "yes",
              $gssapikeyexchange = "no",
                         $usepam = "yes",
                     $acceptenv1 = "LANG LC_CTYPE LC_NUMERIC LC_TIME LC_COLLATE LC_MONETARY LC_MESSAGES",
                     $acceptenv2 = "LC_PAPER LC_NAME LC_ADDRESS LC_TELEPHONE LC_MEASUREMENT",
                     $acceptenv3 = "LC_IDENTIFICATION LC_ALL LANGUAGE",
                     $acceptenv4 = "XMODIFIERS",
           $allowagentforwarding = "yes",
             $allowtcpforwarding = "yes",
                   $gatewayports = "no",
                  $x11forwarding = "yes",
               $x11displayoffset = "10",
                $x11uselocalhost = "yes",
                      $printmotd = "yes",
                   $printlastlog = "yes",
                   $tcpkeepalive = "yes",
                       $uselogin = "no",
         $useprivilegeseparation = "yes",
          $permituserenvironment = "no",
            $clientaliveinterval = "0",
            $clientalivecountmax = "3",
                 $showpatchlevel = "no",
                         $usedns = "yes",
                        $pidfile = "/var/run/sshd.pid",
                    $maxstartups = "10",
                   $permittunnel = "no",
                $chrootdirectory = "none",
                         $banner = "/etc/issue",
                      $subsystem = "sftp    /usr/libexec/openssh/sftp-server",
                          $user1 = "user1",
               $chrootdirectory1 = "chrootdir1",
                  $forcecommand1 = "forcecommand1",
                          $user2 = "user2",
               $chrootdirectory2 = "chrootdir2",
                  $forcecommand2 = "forcecommand2"


){ 
     case $::osfamily {
        Debian: {
            $serviceName = 'ssh'
            $packageName = 'openssh-server'
            $configFile  = '/etc/ssh/sshd_config'
        }
        RedHat: {
            $serviceName = 'sshd'
            $packageName = 'openssh-server'
            $configFile  = '/etc/ssh/sshd_config'
        }
    }
    package { 'openssh-server':
        ensure => 'installed'
    }
    service { 'sshd':
        ensure  => 'running',
        enable  => 'true',
        require => Package[$packageName]
    }
    file { '/etc/ssh/sshd_config':
               owner  => 'root',
               group  => 'root',
                mode  => '0644',
             content  => template('sshd/sshd_config.erb'),
              notify  => Service[$serviceName]
    }
}
